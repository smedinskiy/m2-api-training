<?php declare(strict_types=1);
/**
 * Cyberhull_Blog
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\Blog\Model;

use  Magento\Framework\Api\SearchResults;
use Cyberhull\Blog\Api\Data\ArticleSearchResultsInterface;

/**
 * Class ArticleSearchResults
 */
class ArticleSearchResults extends SearchResults implements ArticleSearchResultsInterface
{

}
