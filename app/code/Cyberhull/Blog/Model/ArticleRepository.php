<?php declare(strict_types=1);
/**
 * Cyberhull_Blog
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\Blog\Model;

use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Cyberhull\Blog\Api\Data\ArticleInterface;
use Cyberhull\Blog\Api\ArticleRepositoryInterface;
use Cyberhull\Blog\Api\Data\ArticleSearchResultsInterface;
use Cyberhull\Blog\Api\Data\ArticleInterfaceFactory as ArticleFactory;
use Cyberhull\Blog\Api\Data\ArticleSearchResultsInterfaceFactory;
use SY\Blog\Model\Article as OriginalArticle;
use SY\Blog\Model\ArticleFactory as OriginalArticleFactory;
use SY\Blog\Model\ResourceModel\Article as OriginalArticleResource;
use SY\Blog\Model\ResourceModel\Article\Collection as OriginalArticleCollection;
use SY\Blog\Model\ResourceModel\Article\CollectionFactory as OriginalArticleCollectionFactory;

/**
 * Class ArticleRepository
 */
class ArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @var ArticleFactory
     */
    private $articleFactory;

    /**
     * ArticleSearchResultsInterfaceFactory
     */
    private $articleSearchResultsFactory;

    /**
     * @var OriginalArticleFactory
     */
    private $originalArticleFactory;

    /**
     * @var OriginalArticleResource
     */
    private $originalArticleResource;

    /**
     * @var OriginalArticleCollectionFactory
     */
    private $originalArticleCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * Constructor
     *
     * @param ArticleFactory                        $articleFactory
     * @param ArticleSearchResultsInterfaceFactory  $articleSearchResultsFactory
     * @param OriginalArticleFactory                $originalArticleFactory
     * @param OriginalArticleResource               $originalArticleResource
     * @param OriginalArticleCollectionFactory      $originalArticleCollectionFactory
     * @param CollectionProcessorInterface          $collectionProcessor
     */
    public function __construct(
        ArticleFactory $articleFactory,
        ArticleSearchResultsInterfaceFactory $articleSearchResultsFactory,
        OriginalArticleFactory $originalArticleFactory,
        OriginalArticleResource $originalArticleResource,
        OriginalArticleCollectionFactory $originalArticleCollectionFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->articleFactory = $articleFactory;
        $this->articleSearchResultsFactory = $articleSearchResultsFactory;
        $this->originalArticleFactory = $originalArticleFactory;
        $this->originalArticleResource = $originalArticleResource;
        $this->originalArticleCollectionFactory = $originalArticleCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritdoc
     */
    public function getById(int $id): ArticleInterface
    {
        $article = $this->loadArticle($id);

        return $this->convertOriginalArticle($article);
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): ArticleSearchResultsInterface
    {
        /** @var OriginalArticleCollection $collection */
        $collection = $this->originalArticleCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var ArticleSearchResultsInterface $searchResults */
        $searchResults = $this->articleSearchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function deleteById(int $id): void
    {
        $article = $this->loadArticle($id);

        try {
            $this->originalArticleResource->delete($article);

        } catch (Exception $ex) {
            throw new LocalizedException(__($ex->getMessage()), $ex);
        }
    }

    /**
     * @inheritdoc
     */
    public function save(ArticleInterface $article): void
    {
        try {
            $originalArticle = $this->convertArticleToOriginal($article);
            $this->originalArticleResource->save($originalArticle);

        } catch (Exception $ex) {
            throw new LocalizedException(__($ex->getMessage()), $ex);
        }
    }

    /**
     * @param int $id
     *
     * @return OriginalArticle
     *
     * @throws NoSuchEntityException
     */
    private function loadArticle(int $id): OriginalArticle
    {
        /** @var OriginalArticle $article */
        $article = $this->originalArticleFactory->create();
        $this->originalArticleResource->load($article, $id);

        if (!$article->getId()) {
            throw new NoSuchEntityException(__('Not found article with ID = %1', $id));
        }

        return $article;
    }

    /**
     * @param OriginalArticle $originalArticle
     *
     * @return ArticleInterface
     */
    private function convertOriginalArticle(OriginalArticle $originalArticle): ArticleInterface
    {
        /** @var ArticleInterface $article */
        $article = $this->articleFactory->create();
        $article->setData($originalArticle->getData());

        return $article;
    }

    /**
     * @param ArticleInterface $article
     *
     * @return OriginalArticle
     */
    private function convertArticleToOriginal(ArticleInterface $article): OriginalArticle
    {
        /** @var OriginalArticle $originalArticle */
        $originalArticle = $this->originalArticleFactory->create();
        $originalArticle->setData($article->getData());

        return $originalArticle;
    }
}
