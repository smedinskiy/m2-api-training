<?php
/**
 * Cyberhull_Blog
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\Blog\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ArticleSearchResultsInterface
 *
 * @api
 */
interface ArticleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get items list
     *
     * @return \Cyberhull\Blog\Api\Data\ArticleInterface[]
     */
    public function getItems();

    /**
     * Set items list
     *
     * @param \Cyberhull\Blog\Api\Data\ArticleInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
