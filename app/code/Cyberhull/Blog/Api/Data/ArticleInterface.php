<?php
/**
 * Cyberhull_Blog
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\Blog\Api\Data;

/**
 * Interface ArticleInterface
 *
 * @api
 */
interface ArticleInterface
{
    /** Article field names */
    const ID = 'id';
    const IMAGE = 'image';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const TEXT = 'text';
    const URL_KEY = 'url_key';
    const ACTIVE = 'active';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get Id
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Set Id
     *
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id): void;

    /**
     * Get Image
     *
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * Set Image
     *
     * @param string $image
     *
     * @return void
     */
    public function setImage(string $image): void;

    /**
     * Get Title
     *
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle(string $title): void;

    /**
     * Get Description
     *
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * Set Description
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription(string $description): void;

    /**
     * Get Text
     *
     * @return string|null
     */
    public function getText(): ?string;

    /**
     * Set Text
     *
     * @param string $text
     *
     * @return void
     */
    public function setText(string $text): void;

    /**
     * Get Url Key
     *
     * @return string|null
     */
    public function getUrlKey(): ?string;

    /**
     * Set Url Key
     *
     * @param string $urlKey
     *
     * @return void
     */
    public function setUrlKey(string $urlKey): void;

    /**
     * Get Active
     *
     * @return int|null
     */
    public function getActive(): ?int;

    /**
     * Set Active
     *
     * @param int $active
     *
     * @return void
     */
    public function setActive(int $active): void;

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Set Created At
     *
     * @param string $createdAt
     *
     * @return void
     */
    public function setCreatedAt(string $createdAt): void;

    /**
     * Get Updated At
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string;

    /**
     * Set Updated At
     *
     * @param string $updatedAt
     *
     * @return void
     */
    public function setUpdatedAt(string $updatedAt): void;
}
