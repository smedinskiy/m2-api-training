<?php
/**
 * Cyberhull_Blog
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\Blog\Api;

use Cyberhull\Blog\Api\Data\ArticleInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Cyberhull\Blog\Api\Data\ArticleSearchResultsInterface;

/**
 * Interface ArticleRepositoryInterface
 *
 * @api
 */
interface ArticleRepositoryInterface
{
    /**
     * Get Article by ID
     *
     * @param int $id
     *
     * @return \Cyberhull\Blog\Api\Data\ArticleInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById(int $id): ArticleInterface;

    /**
     * Get Articles by search criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @return \Cyberhull\Blog\Api\Data\ArticleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): ArticleSearchResultsInterface;

    /**
     * Delete Article by ID
     *
     * @param int $id
     *
     * @return void
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById(int $id): void;

    /**
     * Save Article
     *
     * @param \Cyberhull\Blog\Api\Data\ArticleInterface $article
     *
     * @return void
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ArticleInterface $article): void;
}
