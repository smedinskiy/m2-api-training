<?php declare(strict_types=1);
/**
 * Cyberhull_ProductCmsPage
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\ProductCmsPage\Plugin\Api;

use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Cyberhull\ProductCmsPage\Api\Data\{
    CmsPageInterface,
    CmsPageInterfaceFactory
};

/**
 * Class ProductRepository
 */
class ProductRepository
{
    /**
     * Customer attribute name
     */
    const ATTRIBUTE_NAME = 'cms_page_id';

    /**
     * @var CmsPageInterfaceFactory
     */
    private $cmsPageFactory;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * Constructor
     *
     * @param CmsPageInterfaceFactory $cmsPageFactory
     * @param PageRepositoryInterface $pageRepository
     */
    public function __construct(
        CmsPageInterfaceFactory $cmsPageFactory,
        PageRepositoryInterface $pageRepository
    ) {
        $this->cmsPageFactory = $cmsPageFactory;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param ProductRepositoryInterface $subject
     * @param ProductInterface           $result
     * @param string                     $sku
     *
     * @return ProductInterface
     * @throws LocalizedException
     */
    public function afterGet(
        ProductRepositoryInterface $subject,
        ProductInterface $result,
        string $sku
    ): ProductInterface {
        $attribute = $result->getCustomAttribute(self::ATTRIBUTE_NAME);
        if (!$attribute || !$attribute->getValue()) {
            return $result;
        }

        $page = $this->pageRepository->getById($attribute->getValue());

        /** @var CmsPageInterface $productCmsPage */
        $productCmsPage = $this->cmsPageFactory->create();
        $productCmsPage->setTitle($page->getTitle());
        $productCmsPage->setDescription($page->getIdentifier());

        $result->getExtensionAttributes()->setCmsPage($productCmsPage);

        return $result;
    }
}
