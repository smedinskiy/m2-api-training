<?php
/**
 * Cyberhull_ProductCmsPage
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\ProductCmsPage\Api\Data;

/**
 * Interface CmsPageInterface
 *
 * @api
 */
interface CmsPageInterface
{
    const TITLE = 'title';
    const DESCRIPTION = 'description';

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @param string $title
     *
     * @return void
     */
    public function setTitle(string $title): void;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @param string $description
     *
     * @return void
     */
    public function setDescription(string $description): void;
}
