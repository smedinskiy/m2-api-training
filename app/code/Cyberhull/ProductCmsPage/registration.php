<?php
/**
 * Cyberhull_ProductCmsPage
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Cyberhull_ProductCmsPage',
    __DIR__
);
