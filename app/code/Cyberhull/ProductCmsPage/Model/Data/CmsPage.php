<?php declare(strict_types=1);
/**
 * Cyberhull_ProductCmsPage
 *
 * @copyright Copyright (c) 2019 Cyberhull Inc.
 * @author    Sergey Medinskiy <sergey.medinskiy@cyberhull.com>
 */

namespace Cyberhull\ProductCmsPage\Model\Data;

use Magento\Framework\DataObject;
use Cyberhull\ProductCmsPage\Api\Data\CmsPageInterface;

/**
 * Class CmsPage
 */
class CmsPage extends DataObject implements CmsPageInterface
{
    /**
     * @inheritdoc
     */
    public function getTitle(): ?string
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @inheritdoc
     */
    public function setTitle(string $title): void
    {
        $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritdoc
     */
    public function getDescription(): ?string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritdoc
     */
    public function setDescription(string $description): void
    {
        $this->setData(self::DESCRIPTION, $description);
    }
}
